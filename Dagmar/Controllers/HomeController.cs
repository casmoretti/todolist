﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dagmar.Data.Repository.Implementation;
using Dagmar.Model.Entities;
using Dagmar.Data.UoW.Implementation;

namespace Dagmar.Controllers
{
    public class HomeController : Controller
    {
        UnitOfWork _uow;
        public HomeController()
        {
            _uow = new UnitOfWork();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}