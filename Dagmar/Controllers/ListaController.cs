﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dagmar.Data.UoW.Implementation;
using Dagmar.Model.ViewModels;

namespace Dagmar.Controllers
{
    public class ListaController : Controller
    {
        // GET: Lista
        UnitOfWork _uow;
        public ListaController()
        {
            _uow = new UnitOfWork();
        }

        public ActionResult Index()
        {
            AcoesViewModel vm = new AcoesViewModel()
            {
                Entities = _uow._acoesRepository.GetAll().ToList()
            };

            return View(vm);
        }
    }
}