﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dagmar.Model.Entities.Interfaces
{
    public class Entity<T> where T : struct
    {
        public T Codigo { get; set; }
    }
}
