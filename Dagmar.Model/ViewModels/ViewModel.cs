﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dagmar.Model.ViewModels
{
    public class ViewModel<TEntity> where TEntity : class
    {
        public TEntity Entity;
        public List<TEntity> Entities;
    }
}
