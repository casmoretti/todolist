﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using Dagmar.Model.Entities;

namespace Dagmar.Model.Mapping
{
    public class CategoriasMap : EntityTypeConfiguration<Categories>
    {
        public CategoriasMap()
        {
            this.ToTable("categorias");
            this.HasKey(d => d.Codigo);

            this.Property(d => d.Codigo).HasColumnName("id");
            this.Property(d => d.Nome).HasColumnName("nome");
        }
    }
}
