﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using Dagmar.Model.Entities;

namespace Dagmar.Model.Mapping
{
    public class ResponsaveisMap : EntityTypeConfiguration<Responsaveis>
    {
        public ResponsaveisMap()
        {
            this.ToTable("responsaveis");
            this.HasKey(d => d.Codigo);

            this.Property(d => d.Codigo).HasColumnName("id");
            this.Property(d => d.Nome).HasColumnName("nome");
        }
    }
}
