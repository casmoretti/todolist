﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dagmar.Model.Entities;

namespace Dagmar.Data.Repository.Implementation
{
    public class ResponsaveisRepository : Repository<Responsaveis, long>
    {
        public ResponsaveisRepository(DbContext db) : base(db) { }
    }
}
