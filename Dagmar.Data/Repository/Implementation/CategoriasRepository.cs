﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dagmar.Model.Entities;
using System.Data.Entity;

namespace Dagmar.Data.Repository.Implementation
{
    public class CategoriasRepository : Repository<Categories, long>
    {
        public CategoriasRepository(DbContext db) : base(db) { }

    }
}
