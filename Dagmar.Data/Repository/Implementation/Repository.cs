﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dagmar.Data.Repository.Interface;
using Dagmar.Data.Context;
using System.Data.Entity;
using Dagmar.Model.Entities;

namespace Dagmar.Data.Repository.Implementation
{
    public class Repository<TEntity, TType> : IRepository<TEntity, TType> where TEntity : class
                                                                                    where TType : struct
    {
        DbContext db;

        public Repository(DbContext dbc)
        {
            db = dbc;
        }

        public void Add(TEntity entity)
        {
            IEnumerable<Acoes> acoes = db.Database.SqlQuery<Acoes>("SELECT * FROM ACOES");
            db.Set<TEntity>().Add(entity);
        }

        public void Commit()
        {
            db.SaveChanges();
        }

        public void Rollback()
        {
            db.Dispose();
            GC.SuppressFinalize(this);
        }

        public void Delete(TEntity entity)
        {
            db.Set<TEntity>().Remove(entity);
        }

        public TEntity Get(TType codigo)
        {
            return db.Set<TEntity>().Find(codigo);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return db.Set<TEntity>().ToList();
        }

        public void Update(TEntity entity, TType id)
        {
            throw new NotImplementedException();
        }
    }
}
