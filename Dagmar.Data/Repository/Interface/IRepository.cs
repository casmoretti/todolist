﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dagmar.Data.Repository.Interface
{
    public interface IRepository<TEntity, TType> where TEntity : class
                                                 where TType : struct
    {
        TEntity Get(TType codigo);
        IEnumerable<TEntity> GetAll();
        void Add(TEntity entity);
        void Delete(TEntity entity);
        void Update(TEntity entity, TType id);
        void Commit();
        void Rollback();
    }
}
