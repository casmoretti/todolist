﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dagmar.Data.UoW.Interface
{
    public interface IUnitOfWork
    {
        void Commit();
        void Rollback();
        void ContextImplementation();
    }
}
