﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Dagmar.Data.Context;
using Dagmar.Data.UoW.Interface;
using Dagmar.Data.Repository.Implementation;

namespace Dagmar.Data.UoW.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        
        DbContext _db;
        public AcoesRepository _acoesRepository;
        public CategoriasRepository _categoriasRepository;
        public ResponsaveisRepository _responsaveisRepository;

        public UnitOfWork()
        {
            _db = new Contexto();
            this.ContextImplementation();
        }

        public void Commit()
        {
            _db.SaveChanges();
        }

        public void Rollback()
        {
            _db.Dispose();
            GC.SuppressFinalize(this);
        }

        public void ContextImplementation()
        {
            this._acoesRepository = new AcoesRepository(_db);
            this._categoriasRepository = new CategoriasRepository(_db);
            this._responsaveisRepository = new ResponsaveisRepository(_db);
        }
    }
}
