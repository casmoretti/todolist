﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Dagmar.Model.Entities;
using Dagmar.Model.Mapping;
using System.Diagnostics;
using System.Data.Entity.Migrations;

namespace Dagmar.Data.Context
{
    public class Contexto : DbContext
    {
        public Contexto() 
            : base("name=sql") {
            Database.Log = sql => Debug.Write(sql);
            Database.SetInitializer<Contexto>(null);

        }

        public DbSet<Acoes> Acoes { get; set; }
        public DbSet<Categories> Categorias { get; set; }
        public DbSet<Responsaveis> Responsaveis { get; set; }

        protected override void OnModelCreating(DbModelBuilder mb)
        {
            mb.Configurations.Add(new AcoesMap());
            mb.Configurations.Add(new CategoriasMap());
            mb.Configurations.Add(new ResponsaveisMap());

            Database.SetInitializer<Contexto>(null);
            base.OnModelCreating(mb);
        }
    }
}
